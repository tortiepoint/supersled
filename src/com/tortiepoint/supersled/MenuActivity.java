package com.tortiepoint.supersled;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;

import com.admob.android.ads.AdView;
import com.tortiepoint.supersled.gameobject.GameObject;

public class MenuActivity extends Activity
{
	private AdView adView;

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		LinearLayout layout = new LinearLayout(this);

		adView = new AdView(this);
		adView.setBackgroundColor(Color.BLACK);
		adView.setPrimaryTextColor(Color.WHITE);
		adView.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT,
				LayoutParams.WRAP_CONTENT));

		layout.setOrientation(LinearLayout.VERTICAL);

		layout.addView(adView);
		layout.addView(new MenuView(this));

		setContentView(layout);
	}

	private class MenuView extends SurfaceView implements Updatable,
			SurfaceHolder.Callback
	{
		private Typeface face;
		private Paint textPaint, rightPaint, hiPaint;
		private Bitmap snowman;
		private int snowWidth, snowHeight, frame, midY, width;
		private Rect[] snowRects = new Rect[2];
		private Rect snowRect = new Rect();
		private SharedPreferences prefs = getSharedPreferences("supersled", 0);
		private Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
		private int hi = prefs.getInt("hiscore", 0);
		private boolean soundPref = prefs.getBoolean("sound", true);
		private boolean accelPref = prefs.getBoolean("accel", false);
		private double frameTime;
		private int[] frames = new int[] { 0, 0, 0, 0, 0, 0, 0, 1 };
		private GameLoop _thread;
		private boolean sound = soundPref;
		private boolean accel = accelPref;

		public MenuView(Context context)
		{
			super(context);
			getHolder().addCallback(this);
			textPaint = new Paint();
			face = Typeface.createFromAsset(getAssets(), "wendy.ttf");
			snowman = BitmapFactory.decodeResource(getResources(),
					R.drawable.snowman);
			snowWidth = snowman.getWidth() / 2;
			snowHeight = snowman.getHeight();
			textPaint.setTypeface(face);
			textPaint.setColor(Color.BLACK);
			textPaint.setTextSize(50);
			rightPaint = new Paint(textPaint);
			hiPaint = new Paint(textPaint);
			hiPaint.setColor(Color.RED);
			hiPaint.setTextSize(30);
			rightPaint.setTextAlign(Paint.Align.RIGHT);
			rightPaint.setTextSize(30);
			GameObject.init(getResources());

			for (int i = 0; i < 2; i++)
			{
				snowRects[i] = new Rect(i * snowWidth, 0, (i * snowWidth)
						+ snowWidth, snowHeight);
			}

			SoundPlayer.init(getApplicationContext(), soundPref);
		}

		@Override
		public void surfaceCreated(SurfaceHolder holder)
		{
			midY = getHeight() / 2;
			width = getWidth();

			snowRect.set(10, (getHeight() / 2) - snowHeight, 10 + snowWidth,
					getHeight() / 2);
			_thread = new GameLoop(getHolder(), this);
			_thread.setRunning(true);
			_thread.start();
		}

		@Override
		public void onDraw(Canvas canvas)
		{
			midY = getHeight() / 2;
			width = getWidth();

			canvas.drawColor(Color.GRAY);
			canvas.drawBitmap(snowman, snowRects[frames[frame]], snowRect, null);
			canvas.drawText("SUPER SLED", 20 + snowWidth, getHeight() / 2 - 20,
					this.textPaint);
			canvas.drawText("HI " + hi, 10, 25, hiPaint);
			canvas.drawText("PLAY", width - 20, midY + 10, rightPaint);
			canvas.drawText("CONTROLS", width - 20, midY + 50, rightPaint);
			canvas.drawText("SOUND: " + (sound ? "ON" : "OFF"), width - 20,
					midY + 90, rightPaint);
			canvas.drawText("ACCELEROMETER: " + (accel ? "ON" : "OFF"),
					width - 20, midY + 130, rightPaint);
		}

		@Override
		public boolean onTouchEvent(MotionEvent event)
		{
			float x = event.getX();
			float y = event.getY();

			if (event.getAction() == MotionEvent.ACTION_DOWN)
			{
				if (x > width / 2 && x < width && y > midY + 75
						&& y < midY + 105)
				{
					sound = !prefs.getBoolean("sound", true);
					prefs.edit().putBoolean("sound", sound).commit();
					SoundPlayer.setSound(sound);
					v.vibrate(100);
					SoundPlayer.playJumpScore();
				} else if (x > width / 2 && x < width && y > midY + 115
						&& y < midY + 145)
				{
					accel = !prefs.getBoolean("accel", false);
					prefs.edit().putBoolean("accel", accel).commit();
					v.vibrate(100);
					SoundPlayer.playJumpScore();
				} else if (x < width && x > width / 2 && y > midY - 5
						&& y < midY + 25)
				{
					v.vibrate(100);
					SoundPlayer.playJumpScore();
					startActivity(new Intent(MenuActivity.this,
							SuperSledActivity.class));
				} else if (x > width / 2 && x < width && y > midY + 35
						&& y < midY + 65)
				{
					v.vibrate(100);
					SoundPlayer.playJumpScore();
					startActivity(new Intent(MenuActivity.this,
							ControlsActivity.class));
				}
			}

			return true;
		}

		@Override
		public void update(double elapsed)
		{
			frameTime += elapsed;

			if (frameTime > 250)
			{
				frame = frame == frames.length - 1 ? 0 : frame + 1;
				frameTime = 0;
			}
		}

		@Override
		public void surfaceChanged(SurfaceHolder holder, int format, int width,
				int height)
		{
			// TODO Auto-generated method stub

		}

		@Override
		public void surfaceDestroyed(SurfaceHolder holder)
		{
			// simply copied from sample application LunarLander:
			// we have to tell thread to shut down & wait for it to finish, or
			// else
			// it might touch the Surface after we return and explode
			boolean retry = true;
			_thread.setRunning(false);
			while (retry)
			{
				try
				{
					_thread.join();
					retry = false;
				} catch (InterruptedException e)
				{
					// we will try it again and again...
				}
			}
		}
	}
}
