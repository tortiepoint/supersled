package com.tortiepoint.supersled;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.Window;

import com.tortiepoint.supersled.gameobject.GameObject;
import com.tortiepoint.supersled.gameobject.Obstacle;
import com.tortiepoint.supersled.gameobject.Prize;
import com.tortiepoint.supersled.gameobject.Score;
import com.tortiepoint.supersled.gameobject.Sledder;
import com.tortiepoint.supersled.gameobject.TextObject;

public class SuperSledActivity extends Activity
{
	private boolean ip;
	private SensorManager sensorManager;
	
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
	    GameObject.init(getResources());
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(new Panel(this));
		setVolumeControlStream(AudioManager.STREAM_MUSIC);
	}

	class Panel extends SurfaceView implements SurfaceHolder.Callback, Updatable, SensorEventListener
	{
		private SharedPreferences prefs = getSharedPreferences("supersled", 0);
		private GameLoop _thread;
		private List<GameObject> _objects = new ArrayList<GameObject>();
		private List<GameObject> _removed = new ArrayList<GameObject>();
		private Generator _generator;
		private Sledder _sledder;
		private int _move;
		private boolean _sledderDrawn, gameOver, hiScore, accel;
		private int _sledIndex = -1;
		private double _screenDiv;
		private Paint scorePaint, livesPaint, centerPaint, paint;
		private GameState state = new GameState();
		private Typeface face;
		private Rect hudSrc, hudDest;
		private Score score;
		private int hi;

		public Panel(Context context)
		{
			super(context);
			getHolder().addCallback(this);
		}

		@Override
		public boolean onTouchEvent(MotionEvent event)
		{
			final int action = event.getAction();
			final int code = event.getAction() & MotionEvent.ACTION_MASK;

			if(accel && code == MotionEvent.ACTION_DOWN)
			{
				if(gameOver)
				{
					finish();
				}
				
				_sledder.jump();
				
				return true;
			}
			
			if (code == MotionEvent.ACTION_POINTER_UP)
			{
				final int id = action >> MotionEvent.ACTION_POINTER_ID_SHIFT;
				final int index = event.findPointerIndex(id) == 0 ? 1 : 0;

				if (event.getY(index) < getHeight() * 0.66)
				{
					_sledder.jump();
				} else if (event.getX(index) < _screenDiv)
				{
					_move = 1;
				} else
				{
					_move = 2;
				}

			} else if (code == MotionEvent.ACTION_POINTER_DOWN)
			{
				final int id = action >> MotionEvent.ACTION_POINTER_ID_SHIFT;
				final int index = event.findPointerIndex(id);
				
				if (event.getY(index) < getHeight() * 0.66)
				{
					_sledder.jump();
				} else if (event.getX(index) < _screenDiv)
				{
					_move = 1;
				} else
				{
					_move = 2;
				}
			}
			if (code == MotionEvent.ACTION_DOWN)
			{
				if (gameOver)
				{
					finish();
				}

				if (event.getY() < getHeight() * 0.66)
				{
					_sledder.jump();
				} else if (event.getX() < _screenDiv)
				{
					_move = 1;
				} else
				{
					_move = 2;
				}
			} else if (code == MotionEvent.ACTION_UP)
			{
				_move = 0;
			}

			return true;
		}

		String _log = "";

		@Override
		public void onDraw(Canvas canvas)
		{
			canvas.drawColor(Color.WHITE);

			if (_sledder.isJumping() && _sledder.goingUp())
			{
				if (_sledIndex == -1)
				{
					_sledderDrawn = true;
					_sledder.draw(canvas, paint);
				}

				_sledder.drawJump(canvas);
			}
			if (_sledIndex == -1)
			{
				_sledder.draw(canvas, paint);
				_sledderDrawn = true;
			}
			for (int i = 0; i < _objects.size(); i++)
			{
				_objects.get(i).draw(canvas, paint);

				if (i == _sledIndex)
				{
					_sledder.draw(canvas, paint);
					_sledderDrawn = true;
				}
			}

			if (!_sledderDrawn)
			{
				_sledder.draw(canvas, paint);
			}

			if (_sledder.isJumping() && !_sledder.goingUp())
			{
				_sledder.drawJump(canvas);
			}

			canvas.drawText(state.getScore() + "", 10, 25, scorePaint);
			score.draw(canvas, paint);
			canvas.drawText(state.getLives() + "", getWidth() - 10, 25,
					livesPaint);
			canvas.drawBitmap(GameObject.prizeSprites, hudSrc, hudDest, null);
			_sledderDrawn = false;
			_sledIndex = -1;

			if (gameOver)
			{
				canvas.drawText("GAME OVER", getWidth() / 2, getHeight() / 2,
						centerPaint);

				if (hiScore)
				{
					canvas.drawText("NEW HI SCORE", getWidth() / 2,
							getHeight() / 2 + 20, centerPaint);
				}
			}
		}

		private GameObject _temp;
		private boolean _jump;

		public void update(double elapsed)
		{
			int oSize = _objects.size();
			state.setDeltaY(elapsed);
			_sledder.update(elapsed, _move, _removed, state);

			if (!_sledder.isJumping())
			{
				_jump = false;
			}

			for (int i = 0; i < oSize; i++)
			{
				_temp = _objects.get(i);

				if (_temp.intersects(_sledder))
				{
					if (_temp.active() && !_sledder.isJumping())
					{
						if (_temp instanceof Obstacle && !_sledder.isInvulnerable())
						{
							 _sledder.hit();
							 state.setLives(state.getLives() - 1);

							if (state.getLives() == 0)
							{
								gameOver = true;
								paint.setAlpha(100);

								if (state.getScore() > hi)
								{
									hi = state.getScore();
									prefs.edit().putInt("hiscore", hi).commit();
									hiScore = true;
								}
								_sledder.done();
							}
							_temp.setActive(false);
							break;
						} else if(_temp instanceof Prize)
						{
							((Prize) (_temp)).reward(state);
						}
						
					}

					if (_sledder.isJumping() && !_jump)
					{
						_jump = true;
						state.incrementScore(1000);
						SoundPlayer.playJumpScore();
					}
				}
				if (_temp.behind(_sledder))
				{
					_sledIndex = i;
				}
			}
			for (int i = 0; i < oSize; i++)
			{
				_objects.get(i).update(elapsed, _move, _removed, state);
			}
			_generator.update(_objects, elapsed, _sledder.isMoving()
					&& !gameOver, state);
			_objects.removeAll(_removed);
			_removed.clear();

			score.setScore(state.getNewScore());
			score.update(elapsed, _move, _removed, state);
		}

		@Override
		public void surfaceChanged(SurfaceHolder holder, int format, int width,
				int height)
		{

		}

		@Override
		public void surfaceCreated(SurfaceHolder holder)
		{
			if (!ip)
			{
				ip = true;
				face = Typeface.createFromAsset(getAssets(), "wendy.ttf");
				_screenDiv = getWidth() / 2;
				_generator = new Generator(getWidth(), getBottom(), face);
				_sledder = new Sledder(getWidth() / 2 - 16, 60, 1, getWidth());
				GameObject.sledder = _sledder;
				scorePaint = new Paint();
				scorePaint.setTypeface(face);
				scorePaint.setColor(Color.BLACK);
				scorePaint.setTextSize(30);
				centerPaint = new Paint(scorePaint);
				centerPaint.setColor(Color.BLACK);
				centerPaint.setTextAlign(Paint.Align.CENTER);
				livesPaint = new Paint(scorePaint);
				livesPaint.setTextAlign(Paint.Align.RIGHT);
				paint = new Paint();
				hudSrc = new Rect(0, 0, GameObject.prizeWidth,
						GameObject.prizeHeight);
				hudDest = new Rect(getWidth() - 60, 6, getWidth() - 60
						+ GameObject.prizeWidth, 6 + GameObject.prizeHeight);
				score = new Score(10, 45, face);
				hi = prefs.getInt("hiscore", 0);
				_objects.add(new TextObject(getWidth() / 2, getHeight(),
						"LEVEL 1", face));
			}
			accel = prefs.getBoolean("accel", false);
			
			if(accel)
			{
				sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_NORMAL);
			}
			_thread = new GameLoop(getHolder(), this);
			_thread.setRunning(true);
			_thread.start();
		}

		@Override
		public void surfaceDestroyed(SurfaceHolder holder)
		{
			// simply copied from sample application LunarLander:
			// we have to tell thread to shut down & wait for it to finish, or
			// else
			// it might touch the Surface after we return and explode
			boolean retry = true;
			_thread.setRunning(false);
			
			if(accel)
			{
				sensorManager.unregisterListener(this);
			}
			
			while (retry)
			{
				try
				{
					_thread.join();
					retry = false;
				} catch (InterruptedException e)
				{
					// we will try it again and again...
				}
			}
		}

		public void setRunning(boolean flag)
		{
			_thread.setRunning(flag);
		}

		@Override
		public void onAccuracyChanged(Sensor arg0, int arg1)
		{
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onSensorChanged(SensorEvent event)
		{
			float x = event.values[0];
			
			if(x > 0.4)
			{
				_move = 1;
			}
			else if(x < -0.4)
			{
				_move = 2;
			}
			else
			{
				_move = 0;
			}	
		}
	}
}