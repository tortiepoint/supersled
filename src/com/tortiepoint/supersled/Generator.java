package com.tortiepoint.supersled;

import java.util.List;
import java.util.Random;

import android.graphics.Typeface;

import com.tortiepoint.supersled.gameobject.GameObject;
import com.tortiepoint.supersled.gameobject.Obstacle;
import com.tortiepoint.supersled.gameobject.Prize;
import com.tortiepoint.supersled.gameobject.TextObject;

public class Generator
{
	private int maxInterval = 500;
	private int interval = 500;
	private int scoreTime;
	private boolean levelTrans;
	private double lastObject, startTime, playTime, transTime;
	private int width, height, lastX;
	private Random rand = new Random();
	private Typeface font;
	private final int[][] sprites = new int[][] { { 0 }, { 1 }, { 1 }, { 1 },
			{ 1 }, { 1 }, { 1 }, { 2 }, { 3 }, { 3 }, { 3 }, { 3 }, { 4 },
			{ 4 }, { 5, 6 }, { 7, 8 } };

	private int[][] prizes = new int[][] { { 0 }, { 1 }, { 1 }, { 1 }, { 1 },
			{ 1 }, { 1 }, { 1 }, { 1 }, { 2 }, { 2 }, { 2 }, { 2 }, { 2 },
			{ 2 }, { 2 }, { 2 }, { 2 }, { 2 }, { 2 }, { 2 }, { 2 }, { 2 },
			{ 2 }, { 2 }, { 3 }, { 3 }, { 3 }, { 3 }, { 3 }, { 3 }, { 3 },
			{ 3 }, { 3 }, { 3 }, { 3 }, { 3 }, { 3 }, { 3 }, { 3 } };
	private int prizeLength;

	public Generator(int width, int height, Typeface font)
	{
		this.width = width;
		this.height = height;
		this.font = font;
		prizeLength = prizes.length;
		startTime = System.currentTimeMillis();
	}

	void update(List<GameObject> objects, double elapsed, boolean active,
			GameState state)
	{
		if (levelTrans)
		{
			transTime += elapsed;

			if (transTime > 1000)
			{
				state.incrementLevel();
				objects.add(new TextObject(width / 2, height, "LEVEL "
						+ state.getLevel(), font));
				levelTrans = false;
				transTime = 0;
			}

		} else
		{
			lastObject += elapsed;
			scoreTime += elapsed;
			playTime = System.currentTimeMillis() - startTime;

			if (playTime > 60000 && active)
			{
				maxInterval = Math.min((int) (maxInterval * 0.95), 350);
				state.setSpeed(Math.min((int) (state.getSpeed() * 1.05), 500));
				startTime = System.currentTimeMillis();
				state.incrementScoreMultiplier();
				levelTrans = true;
			}

			if (scoreTime > 1000 && active)
			{
				state.incrementScore(100, false);
				scoreTime = 0;
			}

			if (lastObject > interval && active)
			{
				interval = rand.nextInt(maxInterval);
				lastObject = 0;
				int x = rand.nextInt(width);
				int sprite = rand.nextInt(16);

				if (rand.nextInt(100) > 95)
				{
					while (Math.abs(x - lastX) < 20)
					{
						x = rand.nextInt(width);
					}

					objects.add(new Prize(x, height, prizes[rand
							.nextInt(prizeLength)]));
				} else
				{
					lastX = x;
					objects.add(new Obstacle(x, height, sprites[sprite], 0,
							sprite > 14));
				}
			}
		}
	}
}
