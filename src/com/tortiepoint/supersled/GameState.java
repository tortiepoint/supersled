package com.tortiepoint.supersled;

public class GameState
{
	private int score;
	private int lives = 3;
	private int speed = 100;
	private int deltaY;
	private int scoreMultiplier = 1;
	private int level = 1;
	private int newScore = 0;
	private int xVel = 50;
	
	public int getxVel()
	{
		return xVel;
	}

	public void incrementxVel()
	{
		xVel = (int) Math.min(xVel + 1.05 * xVel, 80);
	}

	public int getLevel()
	{
		return level;
	}

	public void incrementLevel()
	{
		level++;
	}

	public int getScoreMultiplier()
	{
		return scoreMultiplier;
	}

	public void incrementScoreMultiplier()
	{
		scoreMultiplier += 1;
	}

	public int getDeltaY()
	{
		return deltaY;
	}

	public void setDeltaY(double elapsed)
	{
		this.deltaY = ((int) Math.ceil(speed * (elapsed / 1000))) * -1;
	}

	public int getSpeed()
	{
		return speed;
	}

	public void setSpeed(int speed)
	{
		this.speed = speed;
	}

	public int getLives()
	{
		return lives;
	}

	public void setLives(int lives)
	{
		this.lives = lives;
	}

	public int getScore()
	{
		return score;
	}

	public void incrementScore(int score, boolean show)
	{
		this.score += (score * scoreMultiplier);

		if (show)
		{
			newScore += (score * scoreMultiplier);

		}
	}

	public void incrementScore(int score)
	{
		incrementScore(score, true);
	}

	public int getNewScore()
	{
		int result = newScore;

		newScore = 0;

		return result;
	}
}
