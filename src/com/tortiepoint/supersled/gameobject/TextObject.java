package com.tortiepoint.supersled.gameobject;

import java.util.List;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;

import com.tortiepoint.supersled.GameState;

public class TextObject extends GameObject
{
	protected String text;
	protected int locX, locY;
	private static Paint paint;

	public TextObject(int x, int y, String text, Typeface font)
	{
		this.locX = x;
		this.locY = y;
		this.text = text;

		if (paint == null)
		{
			paint = new Paint();
			paint.setTextAlign(Paint.Align.CENTER);
			paint.setTypeface(font);
			paint.setColor(Color.BLACK);
			paint.setTextSize(30);
		}
	}

	@Override
	public void draw(Canvas c, Paint p)
	{
		c.drawText(text, locX, locY, paint);
	}

	@Override
	public void update(double elapsed, int move, List<GameObject> removed,
			GameState state)
	{
		if (sledder.isMoving())
		{
			locY += state.getDeltaY();
		}

		if (locY < 1)
		{
			removed.add(this);
		}
	}

	public boolean intersects(Sledder o)
	{
		return false;
	}

	public boolean behind(Sledder o)
	{
		return false;
	}
}
