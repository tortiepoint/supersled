package com.tortiepoint.supersled.gameobject;

import java.util.List;

import com.tortiepoint.supersled.GameState;
import com.tortiepoint.supersled.R;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

public abstract class GameObject
{
	protected Rect dest;
	protected Rect[] frames;
	protected int frame;
	protected int tileY;
	public static Bitmap obstacleSprites;
	public static Bitmap sledderSprites;
	public static Bitmap prizeSprites;
	public static Rect[] obstacleRects;
	public static Rect[] prizeRects;
	public static Rect livesRect;
	public static int width, height, prizeWidth, prizeHeight;
	public static Sledder sledder;
	protected int fps = 4;
	protected double animateTime;
	protected boolean active = true;

	public static void init(Resources resources)
	{
		GameObject.obstacleSprites = BitmapFactory.decodeResource(
				resources, R.drawable.obstacles_large);
		GameObject.sledderSprites = BitmapFactory.decodeResource(
				resources, R.drawable.sledder_large);
		GameObject.prizeSprites = BitmapFactory.decodeResource(resources,
				R.drawable.prizes);
		GameObject.width = GameObject.obstacleSprites.getWidth() / 9;
		GameObject.height = GameObject.obstacleSprites.getHeight();
		GameObject.prizeWidth = GameObject.prizeSprites.getWidth() / 4;
		GameObject.prizeHeight = GameObject.prizeSprites.getHeight();
	
		obstacleRects = new Rect[9];
		prizeRects = new Rect[4];
		for(int i = 0; i < 9; i ++)
		{
			obstacleRects[i] = new Rect((i * width), 0,
					(i * width) + width, height);
		}
		
		for(int i = 0; i < 4; i ++)
		{
			prizeRects[i] = new Rect((i * prizeWidth), 0,
					(i * prizeWidth) + prizeWidth, prizeHeight);
		}
	}
	
	public GameObject()
	{
		
	}

	public GameObject(int x, int y, int[] frames, Rect[] spriteRects, int width, int height)
	{
		dest = new Rect(x, y, x + width, y + height);
		this.frames = new Rect[frames.length];
		for (int i = 0; i < frames.length; i++)
		{
			this.frames[i] = spriteRects[frames[i]];
		}
	}

	public void update(double elapsed, int move, List<GameObject> removed,
			GameState state)
	{
		animateTime += elapsed;

		if (animateTime > (1000 / fps))
		{
			animateTime = 0;
			frame = frame == frames.length - 1 ? 0 : frame + 1;
		}
	}

	public boolean intersects(Sledder o)
	{
		return o.bounds.intersects(dest.left + 7, dest.centerY() + 4,
				dest.right - 7, dest.bottom - 4);
	}

	public boolean behind(Sledder o)
	{
		return dest.top < o.dest.top && Rect.intersects(o.dest, dest);
	}

	public boolean active()
	{
		return active;
	}

	public void setActive(boolean active)
	{
		this.active = active;
	}

	public abstract void draw(Canvas c, Paint p);
}
