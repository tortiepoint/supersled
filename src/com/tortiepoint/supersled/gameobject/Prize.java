package com.tortiepoint.supersled.gameobject;

import java.util.List;

import com.tortiepoint.supersled.GameState;
import com.tortiepoint.supersled.SoundPlayer;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

public class Prize extends GameObject
{
	private boolean canRemove;
	private int type;

	public Prize(int x, int y, int[] frames)
	{
		super(x, y, frames, prizeRects, prizeWidth, prizeHeight);
		type = frames[0];
	}

	@Override
	public void draw(Canvas c, Paint p)
	{
		c.drawBitmap(prizeSprites, frames[frame], dest, p);
	}

	@Override
	public void update(double elapsed, int move, List<GameObject> removed,
			GameState state)
	{
		super.update(elapsed, move, removed, state);
		if (sledder.isMoving())
		{
			dest.offset(0, state.getDeltaY());
		}

		if (dest.bottom < 1 || canRemove)
		{
			removed.add(this);
		}
	}

	public void reward(GameState state)
	{
		canRemove = true;

		if (type == 0)
		{
			state.setLives(state.getLives() + 1);
		} else
		{
			state.incrementScore(type == 1 ? 2000 : type == 2 ? 500 : 1000);
		}
		
		SoundPlayer.playPrize();
	}
	
	public boolean intersects(Sledder o)
	{
		return Rect.intersects(o.bounds, dest);
	}
}
