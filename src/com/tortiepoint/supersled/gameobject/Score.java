package com.tortiepoint.supersled.gameobject;

import java.util.List;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;

import com.tortiepoint.supersled.GameState;

public class Score extends TextObject
{
	private static Paint scorePaint;
	private double scoreTime;
	private boolean alive;

	public Score(int x, int y, Typeface font)
	{
		super(x, y, "", font);

		if (scorePaint == null)
		{
			scorePaint = new Paint();
			scorePaint.setTextAlign(Paint.Align.LEFT);
			scorePaint.setTypeface(font);
			scorePaint.setColor(Color.RED);
			scorePaint.setTextSize(30);
		}
	}

	@Override
	public void update(double elapsed, int move, List<GameObject> removed,
			GameState state)
	{
		if (alive)
		{
			scoreTime += elapsed;
			
			if (scoreTime > 750)
			{
				alive = false;
				scoreTime = 0;
			}
		}
	}

	@Override
	public void draw(Canvas c, Paint p)
	{
		if (alive)
		{
			c.drawText(text, locX, locY, scorePaint);
		}
	}

	public void setScore(int score)
	{
		if (score != 0)
		{
			alive = true;
			text = String.valueOf(score);
			scoreTime = 0;
		}
	}
}