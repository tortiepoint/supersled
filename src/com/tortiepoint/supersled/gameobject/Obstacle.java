package com.tortiepoint.supersled.gameobject;

import java.util.List;

import com.tortiepoint.supersled.GameState;

import android.graphics.Canvas;
import android.graphics.Paint;

public class Obstacle extends GameObject
{

	private boolean moving;
	private double moveTime;
	private int direction = 0;

	public Obstacle(int x, int y, int[] frames, int srcY, boolean moving)
	{
		super(x, y, frames, obstacleRects, width, height);
		this.moving = moving;
		direction = moving ? 1 : 0;
	}

	@Override
	public void update(double elapsed, int move, List<GameObject> removed, GameState state)
	{
		super.update(elapsed, move, removed, state);
		if (sledder.isMoving())
		{
			dest.offset(direction, state.getDeltaY());
		}
		if (moving)
		{
			moveTime += elapsed;

			if (moveTime > 1500)
			{
				direction *= -1;
				moveTime = 0;
			}
		}
		if (dest.bottom < 1)
		{
			removed.add(this);
		}
	}

	@Override
	public void draw(Canvas c, Paint p)
	{
		c.drawBitmap(obstacleSprites, frames[frame], dest, p);
	}
}
