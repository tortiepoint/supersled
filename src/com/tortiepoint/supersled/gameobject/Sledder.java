package com.tortiepoint.supersled.gameobject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.tortiepoint.supersled.GameState;
import com.tortiepoint.supersled.SoundPlayer;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

public class Sledder extends GameObject
{

	private final int[][] frameSet = new int[][]
	{
	{ 0, 1 },
	{ 2, 3 },
	{ 4, 5 },
	{ 6 },
	{ 7 },
	{ 8 } };
	private Map<Integer, Rect[]> rects = new HashMap<Integer, Rect[]>();
	private int move = 0;
	private boolean hit, jumping, reeling;
	private double downTime, jumpTime, reelTime;
	private Rect jumpRect = new Rect();
	Rect bounds;
	private int yJump = 1;
	private int screenWidth;
	private Paint transPaint;
	
	public Sledder(int x, int y, int srcX, int screenWidth)
	{
		super(x, y, new int[]
		{ 0, 1, 0 }, obstacleRects, width, height);
		this.screenWidth = screenWidth;
		fps = 2;
		int width = sledderSprites.getWidth() / 9;
		int height = sledderSprites.getHeight();

		for (int i = 0; i < frameSet.length; i++)
		{
			Rect[] r = new Rect[frameSet[i].length];
			rects.put(i, r);

			for (int j = 0; j < frameSet[i].length; j++)
			{
				r[j] = new Rect((frameSet[i][j] * width), 0,
						(frameSet[i][j] * width) + width, height);
			}
		}
		bounds = new Rect(dest.left, dest.centerY(), dest.right, dest.bottom);
		transPaint = new Paint();
		transPaint.setAlpha(75);
	}

	public void update(double elapsed, int move, List<GameObject> removed,
			GameState state)
	{
		super.update(elapsed, move, removed, state);

		if (!hit)
		{
			if (jumping)
			{
				jumpTime += elapsed;

				jumpRect.offset(0, -2 * yJump);

				if (jumpTime > 500)
				{
					yJump = -1;
				}
				if (jumpTime > 1000)
				{
					frames = rects.get(move);
					frame = 0;
					jumpTime = 0;
					jumping = false;
					yJump = 1;
				}
			}

			if (this.move != move)
			{
				this.move = move;
				if (!jumping)
				{
					frames = rects.get(move);
					frame = 0;
					
					if(move != 0)
					{
						SoundPlayer.playWoosh();
					}
				}
			}

			int dx = (int) (((Math.ceil(state.getxVel() * (elapsed / 1000)))));

			if (move == 1 && dest.left > 2)
			{
				dest.offset(-dx, 0);
				bounds.offset(-dx, 0);
				jumpRect.offset(-dx, 0);
			} else if (move == 2 && dest.right < screenWidth - 2)
			{
				dest.offset(dx, 0);
				bounds.offset(dx, 0);
				jumpRect.offset(dx, 0);
			}
			
			if(reeling)
			{
				reelTime += elapsed;
				
				if(reelTime > 1000)
				{
					reeling = false;
					reelTime = 0;
				}
			}
		} else
		{
			downTime += elapsed;

			if (downTime > 1000 && !done)
			{
				reeling = true;
				frames = rects.get(move);
				frame = 0;
				hit = false;
				downTime = 0;
			}
		}
	}

	@Override
	public void draw(Canvas c, Paint p)
	{
		c.drawBitmap(sledderSprites, frames[frame], dest, reeling ? transPaint : p);
	}

	public void drawJump(Canvas c)
	{
		c.drawBitmap(sledderSprites, rects.get(5)[0], jumpRect, reeling ? transPaint : null);
	}

	public void hit()
	{
		frames = rects.get(3);
		frame = 0;
		hit = true;
		SoundPlayer.playFall();
	}

	public void jump()
	{
		if (!jumping && !hit)
		{
			frames = rects.get(4);
			frame = 0;
			jumping = true;
			jumpRect = new Rect(dest);
			SoundPlayer.playJump();
		}
	}
	
	private boolean done;
	
	public void done()
	{
		done = true;
	}

	public boolean isMoving()
	{
		return !hit;
	}

	public boolean isJumping()
	{
		return jumping;
	}

	public boolean goingUp()
	{
		return yJump == 1;
	}
	
	public boolean isReeling()
	{
		return reeling;
	}
	
	public boolean isInvulnerable()
	{
		return reeling || hit;
	}
}
