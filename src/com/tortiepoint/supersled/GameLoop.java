package com.tortiepoint.supersled;

import android.graphics.Canvas;
import android.util.Log;
import android.view.SurfaceHolder;


class GameLoop extends Thread
{
	private SurfaceHolder _surfaceHolder;
	private Updatable _panel;
	private boolean _run = false;
	private static double _lastUpdateTime;

	public GameLoop(SurfaceHolder surfaceHolder, Updatable panel)
	{
		_surfaceHolder = surfaceHolder;
		_panel = panel;
	}

	public void setRunning(boolean run)
	{
		_run = run;
		_lastUpdateTime = System.currentTimeMillis();
	}

	@Override
	public void run()
	{
		Canvas c;
		while (_run)
		{
			c = null;
			try
			{
				c = _surfaceHolder.lockCanvas(null);
				synchronized (_surfaceHolder)
				{
					double time = System.currentTimeMillis();
					_panel.update(time - _lastUpdateTime);
					_panel.onDraw(c);
					_lastUpdateTime = time;
				}
			} catch (Exception e)
			{
				Log.e("error", "errr", e);
			} finally
			{
				// do this in a finally so that if an exception is thrown
				// during the above, we don't leave the Surface in an
				// inconsistent state
				if (c != null)
				{
					_surfaceHolder.unlockCanvasAndPost(c);
				}
			}
		}
	}
}