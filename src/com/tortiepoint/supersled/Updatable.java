package com.tortiepoint.supersled;

import android.graphics.Canvas;

public interface Updatable
{
	public void update(double elapsed);
	public void onDraw(Canvas canvas);
}
