package com.tortiepoint.supersled;

import java.util.HashMap;

import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;

public class SoundPlayer
{
	public static final int JUMP = 1;
	public static final int PRIZE = 2;
	public static final int FALL = 3;
	public static final int JUMP_SCORE = 4;
	public static final int WOOSH = 5;

	private static SoundPool soundPool;
	private static HashMap<Integer, Integer> soundPoolMap;
	private static Context context;
	private static boolean soundOn;

	public static void init(Context c, boolean on)
	{
		soundPool = new SoundPool(4, AudioManager.STREAM_MUSIC, 100);
		soundPoolMap = new HashMap<Integer, Integer>();
		soundPoolMap.put(JUMP, soundPool.load(c, R.raw.jump, 1));
		soundPoolMap.put(PRIZE, soundPool.load(c, R.raw.prize, 2));
		soundPoolMap.put(FALL, soundPool.load(c, R.raw.fall, 3));
		soundPoolMap.put(JUMP_SCORE, soundPool.load(c, R.raw.jump_score, 4));
		soundPoolMap.put(WOOSH, soundPool.load(c, R.raw.woosh, 5));
		context = c;
		soundOn = on;
	}

	private static void playSound(int sound)
	{
		playSound(sound, false);
	}

	private static void playSound(int sound, boolean quiet)
	{

		if (soundOn)
		{
			AudioManager mgr = (AudioManager) context
					.getSystemService(Context.AUDIO_SERVICE);
			float streamVolumeCurrent = mgr
					.getStreamVolume(AudioManager.STREAM_MUSIC);
			float streamVolumeMax = mgr
					.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
			float volume = streamVolumeCurrent / streamVolumeMax;

			if (quiet)
			{
				volume /= 20;
			}
			/* Play the sound with the correct volume */
			soundPool.play(soundPoolMap.get(sound), volume, volume, 1, 0, 1f);
		}
	}

	public static void playJump()
	{
		playSound(JUMP);
	}

	public static void playPrize()
	{
		playSound(PRIZE);
	}

	public static void playFall()
	{
		playSound(FALL);
	}

	public static void playJumpScore()
	{
		playSound(JUMP_SCORE);
	}

	public static void playWoosh()
	{
		playSound(WOOSH, true);
	}

	public static void setSound(boolean flag)
	{
		soundOn = flag;
	}
}
