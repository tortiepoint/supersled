package com.tortiepoint.supersled;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.Window;

public class SplashScreenActivity extends Activity
{

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(new SplashView(this));
		/** set time to splash out */
		final int welcomeScreenDisplay = 3000;
		/** create a thread to show splash up to splash time */
		Thread welcomeThread = new Thread()
		{

			int wait = 0;

			@Override
			public void run()
			{
				try
				{
					super.run();
					/**
					 * use while to get the splash time. Use sleep() to increase
					 * the wait variable for every 100L.
					 */
					while (wait < welcomeScreenDisplay)
					{
						sleep(100);
						wait += 100;
					}
				} catch (Exception e)
				{
					System.out.println("EXc=" + e);
				} finally
				{
					/**
					 * Called after splash times up. Do some action after splash
					 * times up. Here we moved to another main activity class
					 */
					startActivity(new Intent(SplashScreenActivity.this,
							MenuActivity.class));
					finish();
				}
			}
		};
		welcomeThread.start();

	}

	private class SplashView extends View
	{
		Bitmap splash = BitmapFactory.decodeResource(getResources(),
				R.drawable.splash);

		public SplashView(Context context)
		{
			super(context);
			// TODO Auto-generated constructor stub
		}

		@Override
		public void onDraw(Canvas canvas)
		{
			canvas.drawColor(Color.WHITE);
			canvas.drawBitmap(splash, (getWidth() / 2)
					- (splash.getWidth() / 2),
					(getHeight() / 2) - (splash.getHeight() / 2) - 50, null);
		}
	}
}
