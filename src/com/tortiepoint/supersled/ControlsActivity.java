package com.tortiepoint.supersled;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;

public class ControlsActivity extends Activity
{
	private Typeface face;
	private boolean accel;

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		SharedPreferences prefs = getSharedPreferences("supersled", 0);
		accel = prefs.getBoolean("accel", false);
		face = Typeface.createFromAsset(getAssets(), "wendy.ttf");
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(new ControlsView(this));
	}

	private class ControlsView extends View
	{
		private static final int EDGE = 30;

		public ControlsView(Context context)
		{
			super(context);
		}

		public void onDraw(Canvas c)
		{
			int width = getWidth();
			int height = getHeight();
			Rect screenRect = new Rect(EDGE, EDGE, width - EDGE, height - EDGE);
			int jumpY = (int) (screenRect.height() * 0.66);
			Rect leftRect = new Rect(EDGE, jumpY, width / 2, height - EDGE);
			Rect rightRect = new Rect(width / 2, jumpY, width - EDGE, height
					- EDGE);
			Paint textPaint = new Paint();
			textPaint.setTextAlign(Paint.Align.CENTER);
			textPaint.setTypeface(face);
			textPaint.setColor(Color.GRAY);
			textPaint.setTextSize(30);
			Paint drawPaint = new Paint();
			drawPaint.setColor(Color.BLACK);
			drawPaint.setStyle(Style.STROKE);
			drawPaint.setStrokeWidth(4);

			c.drawColor(Color.WHITE);
			
			if (accel)
			{
				textPaint.setTextAlign(Paint.Align.LEFT);
				c.drawText("TILT LEFT TO GO LEFT", EDGE, 80, textPaint);
				c.drawText("TILT RIGHT TO GO RIGHT", EDGE, 110, textPaint);
				c.drawText("TAP SCREEN TO JUMP", EDGE, 140, textPaint);
			} else
			{
				c.drawRect(screenRect, drawPaint);
				c.drawLine(EDGE, jumpY, width - EDGE, jumpY, drawPaint);
				c.drawLine(width / 2, jumpY, width / 2, height - EDGE,
						drawPaint);
				c.drawText("JUMP", width / 2,
						height - EDGE - (int) (screenRect.height() * 0.66),
						textPaint);
				c.drawText("GO LEFT", leftRect.centerX(), leftRect.centerY(),
						textPaint);
				c.drawText("GO RIGHT", rightRect.centerX(),
						rightRect.centerY(), textPaint);
			}
			
			textPaint.setColor(Color.RED);
			textPaint.setTextAlign(Paint.Align.CENTER);
			c.drawText("CONTROLS", width / 2, EDGE - 10, textPaint);
			c.drawText("TAP ANYWHERE TO CONTINUE", width / 2, height - EDGE
					+ 20, textPaint);
		}

		@Override
		public boolean onTouchEvent(MotionEvent event)
		{
			finish();

			return true;
		}
	}
}
